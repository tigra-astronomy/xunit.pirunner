﻿// This file is part of the Xunit.PiRunner project
// 
// Copyright © 2016-2016 Tigra Astronomy., all rights reserved.
// 
// File: App.xaml.cs  Last modified: 2016-12-25@17:02 by Tim Long

using System.Reflection;
using Xunit.Runners.UI;

namespace Xunit.PiRunner
    {
    public sealed partial class App : RunnerApplication
        {
        protected override void OnInitializeRunner()
            {
            // tests can be inside the main assembly
            AddTestAssembly(GetType().GetTypeInfo().Assembly);
            // otherwise you need to ensure that the test assemblies will 
            // become part of the app bundle
            // AddTestAssembly(typeof(PortableTests).GetTypeInfo().Assembly);
            }
        }
    }